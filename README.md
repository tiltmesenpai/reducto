# Reducto

Create a discord bot with AWS Lambdas

# Getting Started

In order to use this bot, 3 things must be configured:
- AWS Credentials
- Discord Token
- Your commands

## AWS Credentials

- By default, Reducto uses mix's REPLACE_OS_VARS. If you are running the bot on AWS, simply set the REPLACE_OS_VARS environment variable.
- If you are not running on AWS, you can also set the appropriate environment variables (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,
  and REPLACE_OS_VARS)
- Finally (not recomended), you can hardcode AWS credentials by replacing "${AWS_ACCESS_KEY_ID}" and "${AWS_SECRET_ACCESS_KEY}" in mix.exs.

## Discord Token

- Upload your token to your [AWS SSM Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html) (I highly recommend enabling encryption)
- Change the "Name" key under `token` in mix.exs to your token's name
- If you decided to store your token unencrypted, set "WithDecryption" to false
- (Really not recommended) Reducto can also read your token as a string under the "token" key. You need an AWS account to create the lambdas
  anyways, and Parameter Store is free, so there's really no good reason to not use it.

## Commands
- In mix.exs, fill in the `commands` parameter with pairs of command regexes and lambda function names. As an example, the following
  configuration will call the `helloworld` lambda when the bot sees `!hello`:
  ```elixir
    commands: [
        {~r/!hello/, "helloworld"}
    ]
  ```

- Reducto will pass all named captures from your regex to your lambda. As an example, the following configuration will pass `{"num_pings": "5"}`
  to the `ping` lambda when the bot sees `!ping 5`:
  ```elixir
    commands: [
        {~r/!ping (?<num_pings>\d)/, "ping"}
    ]
  ```

- When Reducto invokes your lambda, the original message will be available under the `message` key. If you use `message` as the name of a
  capture, Reducto will overwrite it with the original message. See https://discordapp.com/developers/docs/resources/channel#message-object for
  details.

- Reducto will reply to the original command with the contents of the `reply` key of the lambda response. Alternately, Reducto will parse and
  send the contents of the `embed` key as an embed. See https://discordapp.com/developers/docs/resources/channel#embed-object for details on this
  object.