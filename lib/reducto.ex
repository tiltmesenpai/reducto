defmodule Reducto do
  use Application
  defmodule Lambda do
    use Alchemy.Events

    def do_command(message) do
      Application.get_env(:reducto, :commands, [])
      |> Enum.find_value(fn({match, lambda}) ->
          case Regex.named_captures(match, message.content) do
            nil -> nil
            args -> 
              AWS.Lambda.invoke(Reducto.client, lambda, Map.put(args, :message, message))
          end
        end
      )
    end

    def command(%Alchemy.Message{author: %Alchemy.User{bot: false}}) do
      alias Alchemy.Embed
      Task.async(fn() -> 
        case do_command(message) do
          {:ok, body, _resp} ->
            reply = cond do
              Map.has_key?(body, "embed") ->
                Enum.reduce(body["embed"], fn({key, val}, embed) ->
                  case key do
                    "author" -> Embed.author(embed, val)
                    "color"  -> if is_integer(val), do: Embed.color(embed, val),
                      else: Embed.color(embed, val |> String.trim_leading("0x") |> Integer.parse(16))
                    "description" -> Embed.description(embed, val)
                    "fields" -> 
                      if is_list(val) do
                        Enum.reduce(val, fn(field, embed) ->
                          Embed.field(embed, Map.get(field, "name", ""), Map.get(field, "value", ""), inline: Map.get(field, "inline", false))
                        end)
                      end
                    "footer" ->
                      if is_map(val) do
                        Embed.footer(embed, text: val["text"], icon_url: val["icon_url"])
                      else
                        Embed.footer(embed, val)
                      end
                    "image"     -> Embed.image(embed, val)
                    "thumbnail" -> Embed.thumbnail(embed, val)
                    "timestamp" -> Embed.timestamp(embed, val)
                    "title"     -> Embed.title(embed, val)
                    "url"       -> Embed.url(embed, val)
                  end
                end)
              Map.has_key?(body, "reply") -> body["reply"]
              true -> nil
            end
            if reply do Alchemy.Client.send_message(message.channel_id, reply) else nil end
          nil -> nil
          end
        end)
    end
    Events.on_message(:command)
  end

  def client do
    struct(AWS.Client, Application.get_env(:reducto, :aws_client))
  end

  def get_token do
    token = Application.get_env(:reducto, :token)
    cond do
      is_map(token) ->
        {:ok, token, _resp} = AWS.SSM.get_parameter(client, token)
        token["Parameter"]["Value"]
      true -> token
    end
  end

  def start(_type, _args) do
    Alchemy.Client.start(get_token())
    use Lambda
  end
end
